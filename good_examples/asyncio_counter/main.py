import asyncio
import functools
import sys

from PyQt5 import uic

from PyQt5.QtWidgets import (
    QWidget,
    QLabel,
    QLineEdit,
    QTextEdit,
    QPushButton,
    QVBoxLayout, QSpinBox, QMessageBox,
)

import qasync
from qasync import asyncSlot, asyncClose, QApplication


class MainWindow(QWidget):
    pushButton: QPushButton
    label: QLabel
    spinBox: QSpinBox

    def __init__(self):
        super().__init__()

        uic.loadUi('./untitled.ui', self)
        self.counter = self.spinBox.value()

        self.pushButton.clicked.connect(self.on_click)

    @asyncClose
    async def closeEvent(self, event):
        pass

    @asyncSlot()
    async def on_click(self):
        QMessageBox.information(self, 'предупреждение', 'отсчёт начинается')

        counter = self.counter
        while counter != 0:
            self.label.setText(f'Осталось {counter} секунд')
            counter -= 1
            await asyncio.sleep(1)

        QMessageBox.information(self, 'предупреждение', 'время истекло')


async def main():
    def close_future(future, loop):
        loop.call_later(10, future.cancel)
        future.cancel()

    loop = asyncio.get_event_loop()
    future = asyncio.Future()

    app = QApplication.instance()
    if hasattr(app, "aboutToQuit"):
        getattr(app, "aboutToQuit").connect(
            functools.partial(close_future, future, loop)
        )

    mainWindow = MainWindow()
    mainWindow.show()

    await future
    return True


if __name__ == "__main__":
    try:
        qasync.run(main())
    except asyncio.exceptions.CancelledError:
        sys.exit(0)
