import logging
import sys
import time

from PyQt5 import uic, QtCore
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QLabel, QVBoxLayout, QPushButton, QLCDNumber, \
    QTextEdit, QMessageBox
from pyqt5_material import apply_stylesheet

from bad_examples.hard_cpu_task.fibonacci import fibonacci
from bad_examples.slow_network.utils import handle_exception
from good_examples.hard_cpu_task.fibonacci import FibonacciWorker
from good_examples.hard_cpu_task.waiting_animation import WaitingAnimation


class Example(QMainWindow):
    """
    Тяжёлые вычислительные задачи тоже требуют времени, а пока мы считаем интерфейс нам недоступен.

    Дабы не замороживать наш интерфейс, мы будем вычислять значение в отдельном **процессе**,
    а не потоке, как в случае с сетью.
    """
    label: QLabel
    pushButton: QPushButton
    textEdit: QTextEdit

    def __init__(self):
        super().__init__()
        self.logger = logging.getLogger('app-logger')
        uic.loadUi('./design.ui', self)

        self.pushButton.clicked.connect(self.calc_start)

        # создаём наш объект, с сигналами, отвечающий за выполнение запросов
        self.fibonacci_worker = FibonacciWorker()
        # подписываемся на сигнал окончания вычислений
        self.fibonacci_worker.calculated.connect(self.calc_end)

        self.task_thread = QtCore.QThread()  # создаём объект потока, в котором будет запускать вычисления\
        # говорим нашей функции фибоначчи запускаться только в отдельном потоке
        self.fibonacci_worker.moveToThread(self.task_thread)

        self.animation_timer = WaitingAnimation(self.label, 'Посчитать число фибоначчи')

    def _call_fibonacci_calc(self, n: int):
        """
        Запуск потока для вычисления функции Фибоначчи.
        """
        QtCore.QMetaObject.invokeMethod(self.fibonacci_worker, 'process',
                                        Qt.QueuedConnection,
                                        QtCore.Q_ARG(int, n))
        self.animation_timer.start()

    def calc_start(self):
        if self.task_thread.isRunning():
            QMessageBox.information(self, 'Расчёт уже запущен', 'Ваше число уже высчитывается, подождите')
            return
        try:
            n = int(self.textEdit.toPlainText())
            self._call_fibonacci_calc(n)
            self.task_thread.start()
        except ValueError:
            QMessageBox.information(self, 'Неверное число',
                                    f'Не удалось преобразовать значение '
                                    f'"{self.textEdit.toPlainText()}" к числу')

    def calc_end(self, n, result):
        self.animation_timer.stop()
        self.task_thread.quit()
        QMessageBox.information(self, 'Расчёт завершён',
                                f'Число Фибоначчи под номером {n} – {result}')


if __name__ == '__main__':
    sys.excepthook = handle_exception
    app = QApplication(sys.argv)

    ex = Example()
    apply_stylesheet(app, theme='light_blue.xml')

    ex.show()

    sys.exit(app.exec())
