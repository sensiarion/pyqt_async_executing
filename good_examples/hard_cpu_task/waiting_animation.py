from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QTextEdit, QLabel


class WaitingAnimation:
    """
    Класс для "анимирования" текста.

    Пока активно, будем добавлять точечки к тексту ...
    """

    def __init__(self, widget: QLabel, start_text: str, points_count: int = 5, interval: int = 1000):
        self.interval = interval
        self.points_count = points_count
        self.widget = widget
        self.start_text = start_text

        self.counter = None
        self.timer = QTimer()
        self.timer.timeout.connect(self.tick)

    def tick(self):
        if self.counter is not None:
            self.counter = (self.counter + 1) % (self.points_count + 1)

            self.widget.setText(self.start_text + '.' * self.counter)

            self.timer.start(self.interval)

    def start(self):
        self.counter = 0
        self.timer.start(self.interval)

    def stop(self):
        self.counter = None
