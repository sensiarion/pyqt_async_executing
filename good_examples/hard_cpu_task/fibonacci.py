from PyQt5 import QtCore
from PyQt5.QtCore import QObject


def fibonacci(n: int):
    """
    Рекурсивная реализация вычисления чисел Фибоначчи.

    Медленная, т.к. не использует кэширование
    """
    if n <= 1:
        return n
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)


class FibonacciWorker(QObject):
    """
    Объект, функцию которого мы запустим в отдельном потоке, дабы он не мешал работе интерфейса
    """
    calculated = QtCore.pyqtSignal(int, int)  # событие, на которое отреагирует наше приложение

    @QtCore.pyqtSlot(int)
    def process(self, n: int):
        result = fibonacci(n)
        self.calculated.emit(n, result)  # порождаем событие, на которое отреагирует наше приложение
