import logging
import sys
import time

from PyQt5 import uic
from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QLabel, QVBoxLayout, QPushButton, QLCDNumber
from pyqt5_material import apply_stylesheet

from bad_examples.slow_network.utils import handle_exception


class Example(QMainWindow):
    """
    Дабы наш счётчик не тормозил всё приложение,
    мы будем с помощью цикла работы Qt периодически вызывать нашу функцию
    """
    lcdNumber: QLCDNumber
    pushButton: QPushButton
    pushButton_2: QPushButton

    TICK_INTERVAL = 1000  # примерный интервал запуска нашей функции в миллисекундах

    def __init__(self):
        super().__init__()
        self.logger = logging.getLogger('app-logger')
        uic.loadUi('./design.ui', self)

        self.pushButton.clicked.connect(self.start_counting)
        self.pushButton_2.clicked.connect(self.stop_counting)

        self.timer = QTimer()  # создаём таймер, который будет вызывать нашу функцию
        self.counter = None
        self.timer.timeout.connect(self.counting_tick)  # прикрепляем функцию к сигналу

    def counting_tick(self):
        """
        Эту функцию раз в N времени будет вызывать pyqt.

        К сожалению, у нас нет гарантий, что это будет происходить в чётко заданные интервалы
        (можем попросить выполнить через 1 секунду, а приложение
        будет что-то выполнять и запустит нашу функцию через 2 секунды).

        Поэтому, мы будем сверять время по выставленной временной метке
        """
        if self.counter:
            lasts = time.time() - self.counter
            self.lcdNumber.display(str(int(lasts)))  # грубое округление, но для нашей задачи само то
            print('прошло', lasts, 'секунд')

            # пока переменная выставлена, перезапускаем таймер, чтобы наша функция снова сработала
            self.timer.start(self.TICK_INTERVAL)

    def start_counting(self):
        """
        Заполняем переменную значения, что бы функция ``counting_tick`` заработала по условию
        """
        self.counter = time.time()
        self.timer.start(self.TICK_INTERVAL)
        self.lcdNumber.display('0')  # сразу сбрасываем значение дисплея, дабы не путать пользователя

    def stop_counting(self):
        """
        Очищаем переменную значения, что бы функция ``counting_tick`` перестала работать по условию
        """
        self.counter = None


if __name__ == '__main__':
    sys.excepthook = handle_exception
    app = QApplication(sys.argv)

    ex = Example()
    apply_stylesheet(app, theme='light_blue.xml')

    ex.show()

    sys.exit(app.exec())
