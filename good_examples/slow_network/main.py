import logging
import sys
import time
from io import BytesIO
from typing import Tuple

import requests
from PyQt5 import uic, QtCore
from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QMessageBox, QLabel, QVBoxLayout, QPushButton
from pyqt5_material import apply_stylesheet

from bad_examples.slow_network.utils import handle_request_error, handle_exception


class NorrisJokeWorker(QObject):
    """
    Специальный объект, выполняющий сетевой запрос.

    Нужен для того, чтобы можно было запустить гео в отдельном потоке и по завершению задачи
    создать сигнал, дабы основное приложение могло на него отреагировать и получить данные
    """
    logger = logging.getLogger('norris-joke-worker')

    loaded = QtCore.pyqtSignal(str)  # событие, передающее шутку, на которое мы сможем подписаться

    def _get_norris_joke(self) -> str:
        """
        Эта функция ходит в сеть за данными
        """
        with handle_request_error(None, self.logger):
            time.sleep(10)
            response = requests.get('https://api.chucknorris.io/jokes/random')
            if response.status_code != 200:
                raise requests.ConnectionError("Неожиданный ответ от сервера")

            return response.json()['value']

    @QtCore.pyqtSlot()
    def process(self):
        joke = self._get_norris_joke()

        self.loaded.emit(joke)  # после того как мы загрузили шутку, порождаем наше событие,
        # чтобы на него отреагировало основной поток приложения


class Example(QMainWindow):
    """
    Если у нас медленная сетевая задача, то мы можем запустить её в отдельном потоке,
    а результат вывести по готовности
    """
    label: QLabel
    label_2: QLabel
    verticalLayout: QVBoxLayout
    pushButton: QPushButton

    def __init__(self):
        super().__init__()
        self.logger = logging.getLogger('app-logger')
        uic.loadUi('./design.ui', self)

        self.pushButton.clicked.connect(self.load_new_joke_start)

        self.init_thread()

    # noinspection PyAttributeOutsideInit
    def init_thread(self):
        # создаём наш объект, с сигналами, отвечающий за выполнение запросов
        self.joke_worker = NorrisJokeWorker()
        # создаём поток, в котором будет выполняться функции нашего объекта
        self.joke_thread = QtCore.QThread()

        self.joke_worker.moveToThread(self.joke_thread)
        self.joke_thread.started.connect(self.joke_worker.process)
        self.joke_worker.loaded.connect(self.load_joke_end)

    def load_new_joke_start(self):
        if self.joke_thread and self.joke_thread.isRunning():
            QMessageBox.information(self, 'Уже загружается', 'Мы ещё ждём предыдущую шутку')
        else:
            self.label_2.setText('Новая шутка подгружается, ожидаем...')
            self.joke_thread.start()

    def load_joke_end(self, joke: str):
        self.verticalLayout.addWidget(QLabel(joke, self))
        self.joke_thread.quit()
        self.label_2.setText('')


if __name__ == '__main__':
    sys.excepthook = handle_exception
    app = QApplication(sys.argv)

    ex = Example()
    apply_stylesheet(app, theme='light_blue.xml')

    ex.show()

    sys.exit(app.exec())
