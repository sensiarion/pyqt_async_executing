import logging
import sys
import time
from io import BytesIO
from typing import Tuple

import requests
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QMessageBox, QLabel, QVBoxLayout, QPushButton
from pyqt5_material import apply_stylesheet

from bad_examples.slow_network.utils import handle_request_error, handle_exception


class Example(QMainWindow):
    label: QLabel
    label_2: QLabel
    verticalLayout: QVBoxLayout
    pushButton: QPushButton

    def __init__(self):
        super().__init__()
        self.logger = logging.getLogger('app-logger')
        uic.loadUi('./design.ui', self)

        self.pushButton.clicked.connect(self.load_new_joke)

    def get_norris_joke(self) -> str:
        with handle_request_error(self, self.logger):
            time.sleep(10)  # чтобы было убедительнее, сэмулируем более долгий ответ от сервера
            response = requests.get('https://api.chucknorris.io/jokes/random')
            if response.status_code != 200:
                raise requests.ConnectionError("Неожиданный ответ от сервера")

            return response.json()['value']

    def load_new_joke(self):
        # т.к. мы последовательно исполняем наш код на python, а в конце функции мы затираем текст
        # приложение не может переключиться на отрисовку и показать этот самый текст
        self.label_2.setText('Вы НИКОГДА не увидите этот текст')
        joke = self.get_norris_joke()
        self.verticalLayout.addWidget(QLabel(joke, self))
        self.label_2.setText('')


if __name__ == '__main__':
    sys.excepthook = handle_exception
    app = QApplication(sys.argv)

    ex = Example()
    apply_stylesheet(app, theme='light_blue.xml')

    ex.show()

    sys.exit(app.exec())
