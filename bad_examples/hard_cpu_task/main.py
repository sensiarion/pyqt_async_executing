import logging
import sys
import time

from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QLabel, QVBoxLayout, QPushButton, QLCDNumber, \
    QTextEdit, QMessageBox
from pyqt5_material import apply_stylesheet

from bad_examples.hard_cpu_task.fibonacci import fibonacci
from bad_examples.slow_network.utils import handle_exception


class Example(QMainWindow):
    """
    Тяжёлые вычислительные задачи тоже требуют времени, а пока мы считаем интерфейс нам недоступен
    """
    label: QLabel
    pushButton: QPushButton
    textEdit: QTextEdit

    def __init__(self):
        super().__init__()
        self.logger = logging.getLogger('app-logger')
        uic.loadUi('./design.ui', self)

        self.pushButton.clicked.connect(self.calc)

    def calc(self):
        try:
            number = int(self.textEdit.toPlainText())
            result = fibonacci(number)
            QMessageBox.information(self, 'Расчёт завершён',
                                    f'Число Фибоначчи под номером {number} – {result}')
        except ValueError:
            QMessageBox.information(self, 'Неверное число',
                                    f'Не удалось преобразовать значение '
                                    f'"{self.textEdit.toPlainText()}" к числу')


if __name__ == '__main__':
    sys.excepthook = handle_exception
    app = QApplication(sys.argv)

    ex = Example()
    apply_stylesheet(app, theme='light_blue.xml')

    ex.show()

    sys.exit(app.exec())
