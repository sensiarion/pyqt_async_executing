def fibonacci(n: int):
    """
    Рекурсивная реализация вычисления чисел Фибоначчи.

    Медленная, т.к. не использует кэширование
    """
    if n <= 1:
        return n
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)
