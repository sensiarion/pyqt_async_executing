import logging
import sys
import time

from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QLabel, QVBoxLayout, QPushButton, QLCDNumber
from pyqt5_material import apply_stylesheet

from bad_examples.slow_network.utils import handle_exception


class Example(QMainWindow):
    """
    Казалось бы, всё что нам нужно – обновлять значение счётчика в цикле.

    Однако т.к. у нас цикл отрисовки графического приложения, всё пойдёт не по плану...
    """
    lcdNumber: QLCDNumber
    pushButton: QPushButton
    pushButton_2: QPushButton

    def __init__(self):
        super().__init__()
        self.logger = logging.getLogger('app-logger')
        uic.loadUi('./design.ui', self)

        self.pushButton.clicked.connect(self.start_counting)
        self.pushButton_2.clicked.connect(self.stop_counting)

        self.counting = False
        self.counter = 0

    def start_counting(self):
        """
        Этот секундомер настолько плох, что вы никогда не увидит что он там насчитал
        """
        self.counting = True
        while self.counting:
            time.sleep(1)  # а вот и магическое число
            self.counter += 1
            self.lcdNumber.display(str(self.counter))
            print('значение счётчика', self.counter)

    def stop_counting(self):
        """
        Дабы остановить счётчик, казалось бы, всего-то нужно поменять значение флага
        :return:
        """
        self.counting = False


if __name__ == '__main__':
    sys.excepthook = handle_exception
    app = QApplication(sys.argv)

    ex = Example()
    apply_stylesheet(app, theme='light_blue.xml')

    ex.show()

    sys.exit(app.exec())
